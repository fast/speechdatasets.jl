# SPDX-License-Identifier: CECILL-2.1

module SpeechDatasets

using JSON
using SpeechFeatures
import MLUtils

export
    # ManifestItem
    Recording,
    Annotation,
    load,

    # Manifest interface
    writemanifest,
    readmanifest,

    # Corpora interface
    download,
    lang,
    name,
    prepare,

    # Corpora
    MultilingualLibriSpeech,
    MINILIBRISPEECH,
    TIMIT,
    INADIACHRONY,
    AVID,
    SPEECH2TEX,

    # Lexicon
    CMUDICT,
    TIMITDICT,
    MFAFRDICT,

    # Dataset
    dataset

include("speechcorpus.jl")
include("manifest_item.jl")
include("manifest_io.jl")
include("dataset.jl")

# Supported corpora
include.("corpora/".*filter(contains(r".jl$"), readdir("src/corpora/")))

include("lexicons.jl")

end
